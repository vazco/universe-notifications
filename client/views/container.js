'use strict';

import {Meteor} from 'meteor/meteor';
import {Template} from 'meteor/templating';
import uniNotifications from '../../lib/notifications.js';


Meteor.startup(function(){
    Template.notificationContainer.helpers({
        getNotifications: function () {
            return uniNotifications.getNotifications(6);
        }
    });

    Template.notificationsDropdownMenu.helpers({
        countUnread: function () {
            return uniNotifications.countUnread();
        },
        useDefaultTemplate: function () {
            return uniNotifications.dropdownMenuTemplate === 'notificationsDropdownMenu';
        },
        getNewDropdownMenuTemplate: function () {
            return uniNotifications.dropdownMenuTemplate;
        }
    });

    Template.notificationsDropdownMenu.onCreated(function () {
        this.subscribe('dropdownNotifications');
    });

    Template.notificationsLinkIcon.helpers({
        countUnread: function () {
            return uniNotifications.countUnread();
        }
    });

    Template.notificationsDropdownMenu.events({
        'click .dropdown-toggle': function () {
            Meteor.call('notificationsSetReceived');
        }
    });

    Template.notificationsLinkIcon.events({
        'click .dropdown-toggle': function () {
            Meteor.call('notificationsSetReceived');
        }
    });

    Template.registerHelper('formatingtime', function (date, format) {
        return moment(date).format(format);
    });
});