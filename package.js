'use strict';

Package.describe({
    summary: 'Notifications API',
    name: 'vazco:universe-notifications',
    version: '2.0.6'
});

Package.onUse(function (api) {

    api.versionsFrom('METEOR@1.3');

    api.use([
        'ecmascript',
        'underscore',
        'momentjs:moment@2.13.1',
        'tracker',
        'session',
        'universe:collection@2.3.9',
        'aldeed:simple-schema@1.5.3',
        'universe:utilities-blaze@1.5.1',
        'universe:utilities@2.3.2',
        'mizzao:user-status@0.6.4'
    ]);

    api.use([
        'templating',
        'less'
    ], 'client');

    api.addFiles([
        'client/views/container.html',
        'client/views/default.html',

        'client/views/notificationsListing.html',
        'client/notifications.less'
    ], 'client');

    api.mainModule('server.js', 'server');
    api.mainModule('client.js', 'client');
    api.mainModule('both.js');
});
