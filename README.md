# Universe Notifications #

### Register notification type ###


```
#!javascript

uniNotifications.register('commentBlog', {
    templateName: 'notificationCommentItem',
    entityCollection: 'Blog',
    titleField: 'title',
    relatedUser: function (entity) {
        var relatedUsersIds = [];
        var currentUserId = Meteor.userId();
        UniComments.Comments.find({containerId: entity._id}, {
            sort: {createdAt: -1},
            fields: {containerId: 1, userId: 1}
        }).forEach(function (comment) {
            if (comment.userId !== currentUserId) {
                relatedUsersIds.push(comment.userId);
            }
        });

        if (entity.ownerId !== currentUserId) {
            relatedUsersIds.push(entity.ownerId);
        }
        return _.uniq(relatedUsersIds);
    },
    getURL: function (notification) {
        return Meteor.absoluteUrl('blog/' + notification.entityId);
    }
});
```

### Item Template example ###


```
#!javascript

<template name="notificationCommentItem">
    <div class="row uni-notifications-item-wrapper">
        <div class="col-xs-12 uni-notifications-item-container">
            <div class="inline-block">
                {{#getUser authorInfo._id}}
                    {{#if avatar}}
                        <img src="{{avatar.url}}" class="img-circle" alt="">
                    {{else}}
                        <img src="/img/160x160.gif" alt="" class="img-circle styled">
                    {{/if}}
                {{/getUser}}
            </div>
            <div class="inline-block mls">
                <a href="{{getURL}}">
                    <strong>{{authorInfo.name}}</strong> commented <span class="seledin-text">{{entityTitle}}</span>
                    <span class="time-created-label">{{formatingtime createdAt 'MMM DD, YYYY hh:mm'}}</span>
                </a>
            </div>
        </div>
    </div>
</template>
```

### topbar default template ###


```
#!javascript

{{> notificationsDropdownMenu}}
```

### Create Notification (only Server side)###


```
#!javascript

uniNotifications.create('commentBlog', userId, doc._id);
```

### Subscription ###

You must add subcription to Your rotuter controller. This is example:


```
#!javascript

RouteCtrls.NotificationsListing = RouteCtrls.Basic.extend({
    subscriptions: function () {
        return [
            Meteor.subscribe('allNotifications')
        ];
    },
    data: function () {
        return {
            notifications: uniNotifications.getNotifications()
        };
    }
});
```