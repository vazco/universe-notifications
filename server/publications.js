'use strict';

import {Meteor} from 'meteor/meteor';
import UniNotifications from '../lib/collection.js';


Meteor.publish('dropdownNotifications', function () {
    return UniNotifications.find({
        relatedWith: this.userId,
        entityId: {$exists: true}
    }, {
        sort: {createdAt: -1},
        limit: 6
    });
});

Meteor.publish(null, function () {
    if (this.userId) {
        return UniNotifications.find({relatedWith: this.userId, received: false}, {
            fields: {
                received: 1,
                relatedWith: 1
            }
        });

    }
    this.ready();
});

Meteor.publish('allNotifications', function () {
    if (this.userId) {
        return UniNotifications.find({relatedWith: this.userId}, {sort: {createdAt: -1}});
    }
    this.ready();
});
