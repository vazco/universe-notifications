'use strict';

import {Meteor} from 'meteor/meteor';
import {_} from 'meteor/underscore';
import UniNotifications from '../lib/collection';
import uniNotifications from '../lib/notifications';


Meteor.methods({
    'notificationsSetReceived': function () {
        if (this.userId) {
            UniNotifications.update({relatedWith: this.userId, received: false}, {
                $set: {
                    received: true,
                    receivedAt: new Date()
                }
            }, {multi: true});

            _.each(uniNotifications.registredNotifications || {}, rn => {
                if (!rn.removeReadAfter) {
                    return;
                }
                Meteor.setTimeout(() => {
                    UniNotifications.remove({
                        notificationType: rn.name,
                        relatedWith: this.userId,
                        received: true
                    });
                }, parseInt(rn.removeReadAfter) || 1);
            });
                
        }
    }
});
