'use strict';

import uniNotifications from './notifications.js';


var authorInfoSchema = new SimpleSchema({
    _id: {
        type: String,
        optional: true
    },
    name: {
        type: String,
        optional: true
    },
    surname: {
        type: String,
        optional: true
    }
});

var notificationSchema = new SimpleSchema({
    createdAt: {
        type: Date,
        autoValue: function () {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return {$setOnInsert: new Date};
            }
            this.unset();
        }
    },
    receivedAt: {
        type: Date,
        optional: true
    },
    received: {
        type: Boolean
    },
    createdBy: {
        type: String
    },
    entityId: {
        type: String
    },
    entityTitle: {
        type: String,
        optional: true
    },
    relatedWith: {
        type: String
    },
    notificationType: {
        type: String
    },
    authorInfo: {
        type: authorInfoSchema,
        optional: true
    },
    info: {
        type: Object,
        blackbox: true,
        optional: true
    }
});

const UniNotifications = new UniCollection('uniNotifications');
UniNotifications.setSchema(notificationSchema);

if (Meteor.isServer) {
    UniNotifications.allow({
        insert: function (userId) {
            return !!userId;
        },
        update: function (userId, doc) {
            return userId && doc.createdBy === userId;
        },
        remove: function (userId, doc) {
            return userId && (doc.createdBy === userId || doc.relatedWith === userId);
        }
    });
}


if (Meteor.isClient) {
    UniNotifications.docHelpers({
        getItemTemplate: function () {
            return uniNotifications.registredNotifications[this.notificationType].itemTemplate;
        },
        getListingTemplateName: function () {
            return uniNotifications.registredNotifications[this.notificationType].listingTemplateName;
        },
        getURL: function (type) {
            return uniNotifications.registredNotifications[this.notificationType].getURL(this, type);
        }
    });
}

export default UniNotifications;
