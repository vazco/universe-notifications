'use strict';

import UniNotifications from './collection.js';
import {UniUtils} from 'meteor/universe:utilities';
import {UniUsers} from 'meteor/universe:collection';


let uniNotifications = {
    registredNotifications: {},
    dropdownMenuTemplate: 'notificationsDropdownMenu',
    register: function (name, settings) {
        if (typeof settings === 'object' && name) {
            this.registredNotifications[name] = {
                name: name,
                itemTemplate: settings.templateName,
                listingTemplateName: settings.listingTemplateName || settings.templateName,
                entityCollection: settings.entityCollection,
                relatedUserField: settings.relatedUserField,
                relatedUser: settings.relatedUser,
                getURL: settings.getURL,
                titleField: settings.titleField,
                removeReadAfter: settings.removeReadAfter || false
            };
        } else {
            console.warn('"settings" in uniNotifications.register(settings); is not Object');
        }
    },
    registerDropdownMenuTemplate (templateName) {
        if (templateName) {
            return this.dropdownMenuTemplate = templateName;
        }
    },
    create: function (type, userId, entityId, additionalInfo) {
        if (!Meteor.isServer) {
            return;
        }
        var settings;
        var entity;
        var relatedUser;
        var title;
        var Collection;
        var author;
        var authorInfo;

        if (Match.test([userId, type, entityId], [String])) {
            settings = this.getNotificationSettings(type);
            additionalInfo = additionalInfo || {};
            author = UniUsers.findOne({_id: userId});
            authorInfo = {
                _id: author._id,
                name: author.getName()
            };
            if (settings) {
                Collection = settings.entityCollection;

                if (Collection) {
                    entity = Collection.findOne({_id: entityId});
                }

                relatedUser = settings.relatedUser(entity, additionalInfo);
                if (!entity && !relatedUser) {
                    console.warn('Create notification FAILED. Entity is undefined (_id: ' + entityId + ')');
                }
                if (relatedUser) {
                    if (entity) {
                        title = settings.titleField ? entity[settings.titleField] : entity.title || entity.name;
                    }
                    relatedUser = _.isArray(relatedUser) ? relatedUser : [relatedUser];
                    relatedUser.forEach(function (relatedUserId) {
                        if (relatedUserId) {
                            relatedUserId = UniUtils.getIdIfDocument(relatedUserId);
                            UniNotifications.insert({
                                received: false,
                                createdBy: userId,
                                authorInfo: authorInfo,
                                entityId: entityId,
                                entityTitle: title,
                                relatedWith: relatedUserId,
                                notificationType: type,
                                info: additionalInfo
                            });
                        }
                    });
                } else {
                    console.warn('Create notification FAILED. This entity dont have related user (_id: ' + entityId + ') ');
                }

            } else {
                console.warn('Create notification FAILED. Notification type: ' + type + ' doesnt exist');
            }
        } else {
            console.warn('Create notification FAILED. One of parameters is undefined (type, userId, entityId)');
        }
    },
    getNotificationSettings: function (type) {
        return this.registredNotifications[type];
    },
    getNotifications: function (limit) {
        var userId = Meteor.userId();
        var notificationsLimit = limit ? limit : 0;
        return UniNotifications.find({relatedWith: userId, entityId: {$exists: true}}, {
            sort: {createdAt: -1},
            limit: notificationsLimit
        });
    },
    countUnread: function () {
        var userId = Meteor.userId();
        return UniNotifications.find({relatedWith: userId, received: false}).count();
    },
    setReceived: function (notificationId) {
        UniNotifications.update({_id: notificationId}, {$set: {received: true, receivedAt: new Date()}});
    },
    getEntity: function (notificationType, entityId) {
        var Collection = this.registredNotifications[notificationType].entityCollection;

        if (!Collection) {
            return console.error(notificationType + ' does not have a corresponding collection');
        }

        return Collection.findOne({_id: entityId});
    }
};

export default uniNotifications;
